<?php

namespace App\Repository;

use App\Entity\FacilityReport;
use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FacilityReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method FacilityReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method FacilityReport[]    findAll()
 * @method FacilityReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacilityReportRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, FacilityReport::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(FacilityReport $entity, bool $flush = true): void {
        return; //we do not allow adding
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(FacilityReport $entity, bool $flush = true): void {
        return; //we do not allow removal
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findMinDate() {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT min(far.gas_day_start) as start 
            FROM App\Entity\FacilityReport far'
        );

        // returns an array of Product objects
        return $query->getResult();
    }

    public function findChanged($year) {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT far.gas_day_start as start 
            FROM App\Entity\FacilityReport far
             WHERE far.updated_at >= :now AND far.gas_day_start LIKE :year'
            )
            ->setParameter('now', Carbon::now()->format('Y-m-d'))
            ->setParameter('year', $year.'%')
        ;

        // returns an array of Product objects
        return $query->getResult();
    }


    public function getListing($start, $last): array {
        $conn = $this->getEntityManager()
            ->getConnection();
        $q = [
            0 => 'continent', 1 => 'country', 2 => 'company', 3 => 'facility',
        ];
        $sql_array = [];
        foreach ($q as $k => $e) {
            //$string = "(SELECT IF(country.type = 2, 'ne', IF(country.type = 1, 'eu', 'ai')) AS type, ";
            //$group_by = "country.type";
            $string = "(SELECT IF(country.europe = 0, 'ne', IF(country.europe = 1, 'eu', 'ai')) AS type, ";
            $group_by = "far.gas_day_start, country.europe";
            if ($k >= 1) {
                $string .= "country.id AS country_id, ";
                $group_by = "far.gas_day_start, country.id";
            } else {
                $string .= "'' AS country_id, ";
            }
            if ($k >= 2) {
                $string .= "company.id AS company_id, ";
                $group_by = "far.gas_day_start, company.id";
            } else {
                $string .= "'' AS company_id, ";
            }
            if ($k >= 3) {
                $string .= "facility.id AS facility_id, ";
                $group_by = "far.gas_day_start, facility.id";
            } else {
                $string .= "'' AS facility_id, ";
            }
            $string .= "far.gas_day_start AS gasDayStart, 
SUM(far.storage_value) AS gasInStorage, 
SUM(far.injection_value) AS injection, 
SUM(far.withdrawal_value) AS withdrawal, 
SUM(far.technical_capacity_value) AS workingGasVolume, 
SUM(far.dtmti_value) AS injectionCapacity, 
SUM(far.dtmtw_value) AS withdrawalCapacity, 
IF(MAX(far.estimated), 'E', 'C') AS status,
IF(SUM(far.technical_capacity_value) = 0, 0, 
    (
        (
            SUM(far.injection_value) - SUM(far.withdrawal_value)
        ) / (
            SUM(far.technical_capacity_value) * 1000
        )
    ) * 100
) AS trend,
IF(
    (SUM(far.storage_value) / SUM(far.technical_capacity_value) * 100) > 100, 
    100, 
    (SUM(far.storage_value) / SUM(far.technical_capacity_value) * 100)
) AS full,
GROUP_CONCAT(CAST(far.notifications as CHAR) SEPARATOR ',') AS notifications
FROM rrm_live.countries country
  LEFT JOIN rrm_live.facilities facility ON facility.country_id = country.id
  LEFT JOIN remit.report_facility_storage far ON far.facility_identifier_id = facility.id
  LEFT JOIN rrm_live.eic eic ON facility.id = eic.facility_id
  LEFT JOIN rrm_live.companies company ON far.facility_operator_identifier_id = company.id 
  WHERE company.type = 'SSO' AND far.gas_day_start >= '" . $start . "' AND far.gas_day_start <= '" . $last . "'
  GROUP BY " . $group_by . "
  ORDER BY far.gas_day_start ASC, country.europe ASC, country.name ASC, company.name ASC, facility.name ASC
  )
  ";
            $sql_array[] = $string;
        }
        $sql = 'SELECT type, country_id, company_id, facility_id, gasDayStart, gasInStorage, injection, withdrawal, workingGasVolume, injectionCapacity, withdrawalCapacity, status, trend, full, notifications FROM (' . implode(' UNION ', $sql_array) . ') AS q ORDER BY type ASC, country_id ASC, company_id ASC, facility_id ASC, gasDayStart DESC';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }


    // /**
    //  * @return FacilityReport[] Returns an array of FacilityReport objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    /*
    public function findOneBySomeField($value): ?FacilityReport
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}