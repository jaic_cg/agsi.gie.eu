<?php

namespace App\Repository;

use App\Entity\Facility;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Facility|null find($id, $lockMode = null, $lockVersion = null)
 * @method Facility|null findOneBy(array $criteria, array $orderBy = null)
 * @method Facility[]    findAll()
 * @method Facility[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacilityRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Facility::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Facility $entity, bool $flush = true): void {
        return;
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Facility $entity, bool $flush = true): void {
        return;
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getListing($year): array {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "SELECT 
/*IF(c2.type = 2, 'ne', IF(c2.type = 1, 'eu', 'ai')) AS type,*/
IF(c2.europe = 0, 'ne', IF(c2.europe = 1, 'eu', 'ai')) AS type,
c2.id AS country_id,
c2.name AS country,
c2.slug AS country_slug,
c3.consumption AS country_consumption,       
c1.id AS company_id,
c1.short_name AS company,
c1.eic AS company_eic,
f.id AS facility_id,
f.name AS facility,
e.eic AS facility_eic
/*FROM remit.facility f
LEFT JOIN remit.company c1 ON f.company_id = c1.id
LEFT JOIN remit.country c2 ON f.country_id = c2.id
LEFT JOIN remit.eic e ON f.id = e.facility_id
WHERE c1.web_status < 10 AND f.web_status < 10 AND c1.type IN (0,1)*/
FROM rrm_live.facilities f
LEFT JOIN rrm_live.companies c1 ON f.company_id = c1.id
LEFT JOIN rrm_live.countries c2 ON f.country_id = c2.id
LEFT JOIN rrm_live.country_consumption c3 ON c3.country_id = c2.id AND c3.year = '" . $year . "'
LEFT JOIN rrm_live.eic e ON f.id = e.facility_id
WHERE c1.hide = 0 AND f.hide = 0 AND c1.type IN ('SSO')
GROUP BY f.id ORDER BY c2.europe DESC, c2.name ASC, c1.name ASC, f.name ASC, e.eic ASC;";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getListingByCompanyId($year, $company_id): array {
        $conn = $this->getEntityManager()
            ->getConnection();
        $sql = "SELECT 
/*IF(c2.type = 2, 'ne', IF(c2.type = 1, 'eu', 'ai')) AS type,*/
IF(c2.europe = 0, 'ne', IF(c2.europe = 1, 'eu', 'ai')) AS type,
c2.id AS country_id,
c2.name AS country,
c2.slug AS country_slug,
c3.consumption AS country_consumption,       
c1.id AS company_id,
c1.short_name AS company,
c1.eic AS company_eic,
f.id AS facility_id,
f.name AS facility,
e.eic AS facility_eic
/*FROM remit.facility f
LEFT JOIN remit.company c1 ON f.company_id = c1.id
LEFT JOIN remit.country c2 ON f.country_id = c2.id
LEFT JOIN remit.eic e ON f.id = e.facility_id
WHERE c1.web_status < 10 AND f.web_status < 10 AND c1.type IN (0,1)*/
FROM rrm_live.facilities f
LEFT JOIN rrm_live.companies c1 ON f.company_id = c1.id
LEFT JOIN rrm_live.countries c2 ON f.country_id = c2.id
LEFT JOIN rrm_live.country_consumption c3 ON c3.country_id = c2.id AND c3.year = '" . $year . "'
LEFT JOIN rrm_live.eic e ON f.id = e.facility_id
WHERE c1.id = '" . $company_id . "'
GROUP BY f.id ORDER BY c2.europe DESC, c2.name ASC, c1.name ASC, f.name ASC, e.eic ASC;";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }


    // /**
    //  * @return Facility[] Returns an array of Facility objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    /*
    public function findOneBySomeField($value): ?Facility
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}