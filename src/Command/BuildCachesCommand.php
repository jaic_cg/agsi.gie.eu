<?php
namespace App\Command;

use App\Entity\Facility;
use App\Entity\FacilityReport;
use Carbon\Carbon;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Cache\ItemInterface;

class BuildCachesCommand extends Command {
    protected static $defaultName = 'app:caches';
    protected $doctrine, $io;

    public function __construct(ManagerRegistry $doctrine) {
        parent::__construct();
        $this->doctrine = $doctrine;
    }

    protected function configure() {
        $this
            ->setDescription('Build caches for AGSI');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $this->io = new SymfonyStyle($input, $output);
        self::startCache();
        return 0;
    }

    public function startCache() {
        $io = $this->io;
        $type = 'storage';

        $now = Carbon::now()->setTimezone('utc');
        $current_gas_dayCarbon = ($now->format('H:i') < '18:30') ? $now->subDays(2) : Carbon::yesterday();
        $current_gas_day = $current_gas_dayCarbon->format('Y-m-d');
        try {
            //get min date
            $result = $this->doctrine->getRepository(FacilityReport::class)->findMinDate();
            if (count($result) !== 1) {
                $io->error('Resource not found');
            }
            $start = $result[0]['start'];
            $dates = self::arrayKeyDates($start, $current_gas_day);
            //going to do the loop-di-loop
            $startCarbon = Carbon::parse($start);
            $start_year = $startCarbon->format('Y');
            $start = $startCarbon->format('Y-m-d');
            $current_year = $current_gas_dayCarbon->format('Y');
            $i = $start_year;

            $cache = new FilesystemAdapter();
            while ($i <= $current_year) {
                //2009 - 2022
                $io->note('started at: ' . Carbon::now()->format('Y-m-d H:i'));
                $io->note('year: ' . $i);
                $forget_cache = self::forgetThisCache($i);
                //forget the cache if there was an update
                if ($forget_cache) {
                    try {
                        $cache->delete('reports_sso_' . $i);
                    } catch (InvalidArgumentException $e) {
                        continue;
                    }
                }
                $data = $cache->get('reports_sso_'.$i, function (ItemInterface $item) use ($dates, $type, $start_year, $i, $startCarbon, $start, $current_gas_day) {
                    $item->expiresAfter(3600); //1 hour
                    if ($start_year !== $i) {
                        //not start date > set to beginning of year
                        $start = Carbon::parse($i . '-01-01')->format('Y-m-d');
                    }
                    $last = Carbon::parse($i . '-12-31')->format('Y-m-d');
                    if ($last > $current_gas_day) {
                        $last = $current_gas_day;
                    }
                    $dates_looped = self::arrayKeyDates($start, $last);
                    self::generateCompleteListing($dates_looped, $i);
                    self::getStorageDBValues($dates_looped, $start, $last);
                    return $dates_looped;
                });
                $dates = array_merge($dates, $data);
                $i++;
            }
            $io->note('done at: ' . Carbon::now()->format('Y-m-d H:i'));
        } catch (Exception | InvalidArgumentException $exception) {
            dd($exception->getFile(), $exception->getLine(), $exception->getMessage());
        }
    }

    public function forgetThisCache($i): bool {
        //see if anything changed for this year cache
        $checked = $this->doctrine->getRepository(FacilityReport::class)->findChanged($i);
        return (count($checked) > 0);
    }

    public static function getCodes(): array {
        return [
            'eu' => ['name' => 'EU', 'id' => 1],
            //'ne' => ['name' => 'Non-EU', 'id' => 2],
            'ne' => ['name' => 'Non-EU', 'id' => 0],
            //'ai' => ['name' => 'Additional Information', 'id' => 3],
            'ai' => ['name' => 'Additional Information', 'id' => 2],
        ];
    }

    public function generateCompleteListing(&$dates, $year) {
        $io = $this->io;
        $codes = self::getCodes();
        //get all countries, companies and facilities
        $result = $this->doctrine->getRepository(Facility::class)->getListing($year);
        if (count($result) === 0) {
            $io->error('Resource not found');
        }
        $dates_chunk = array_chunk($dates, 30, true);
        foreach ($dates_chunk as $chunk) {
            foreach ($chunk as $d => $a) {
                $dates[$d] = [];
                $data = [
                    'gasDayStart' => $d,
                    'gasInStorage' => '-',
                    'consumption' => '-',
                    'consumptionFull' => '-',
                    'injection' => '-',
                    'withdrawal' => '-',
                    'workingGasVolume' => '-',
                    'injectionCapacity' => '-',
                    'withdrawalCapacity' => '-',
                    'status' => 'N',
                    'trend' => '-',
                    'full' => '-',
                    'info' => [],
                ];
                foreach ($result as $r) {
                    $r = (object)$r;
                    //[gas_day_start][continent][country][company][facility]
                    //continent
                    if (!array_key_exists($r->type, $dates[$d])) {
                        $dates[$d][$r->type] = array_merge([
                            'name' => $codes[$r->type]['name'],
                            'code' => $r->type,
                            'url' => $r->type,
                        ], $data, ['children' => []]);
                    }
                    //country
                    if (!array_key_exists($r->country_id, $dates[$d][$r->type]['children'])) {
                        $dates[$d][$r->type]['children'][$r->country_id] = array_merge([
                            'name' => $r->country,
                            'code' => $r->country_slug,
                            'url' => $r->country_slug,
                        ], $data, ['children' => []]);
                        $dates[$d][$r->type]['children'][$r->country_id]['consumption'] =
                            (is_null($r->country_consumption) ? '-' : $r->country_consumption);
                        $dates[$d][$r->type]['children'][$r->country_id]['consumptionFull'] =
                            '0';
                    }
                    //company
                    if (!array_key_exists($r->company_id, $dates[$d][$r->type]['children'][$r->country_id]['children'])) {
                        $dates[$d][$r->type]['children'][$r->country_id]['children'][$r->company_id] = array_merge([
                            'name' => $r->company,
                            'code' => $r->company_eic,
                            'url' => $r->company_eic . '/' . $r->country_slug,
                        ], $data, ['children' => []]);
                    }
                    //facility
                    if (!array_key_exists($r->facility_id, $dates[$d][$r->type]['children'][$r->country_id]['children'][$r->company_id]['children'])) {
                        $dates[$d][$r->type]['children'][$r->country_id]['children'][$r->company_id]['children'][$r->facility_id] = array_merge([
                            'name' => $r->facility,
                            'code' => $r->facility_eic,
                            'url' => $r->facility_eic . '/' . $r->country_slug . '/' . $r->company_eic,
                        ], $data);
                    }
                    unset($dates[$d][$r->type]['children'][$r->country_id]['children'][$r->company_id]['consumption']);
                    unset($dates[$d][$r->type]['children'][$r->country_id]['children'][$r->company_id]['consumptionFull']);
                    unset($dates[$d][$r->type]['children'][$r->country_id]['children'][$r->company_id]['children'][$r->facility_id]['consumption']);
                    unset($dates[$d][$r->type]['children'][$r->country_id]['children'][$r->company_id]['children'][$r->facility_id]['consumptionFull']);
                }
                unset($data);
            }
        }
    }

    public function getStorageDBValues(&$dates, $start, $last) {
        $io = $this->io;
        //dd(count($result));
        $result = $this->doctrine->getRepository(FacilityReport::class)->getListing($start, $last);
        if (count($result) === 0) {
            $io->error('Resource not found');
        }
        $result_chunks = array_chunk($result, 30, true);
        foreach ($result_chunks as $result_chunk) {
            foreach ($result_chunk as $r) {
                $r = (object)$r;
                $info = array_unique(preg_filter('/^/', '/news/', array_filter(explode(',', str_replace(['[', ']'], '', $r->notifications)))));
                $data = [
                    "gasDayStart" => $r->gasDayStart,
                    "gasInStorage" => (string)round($r->gasInStorage, 4), //FORMAT(SUM(far.storage_value), 4
                    "injection" => (string)round($r->injection, 2),
                    "withdrawal" => (string)round($r->withdrawal, 1),
                    "workingGasVolume" => (string)round($r->workingGasVolume, 4),
                    "injectionCapacity" => (string)round($r->injectionCapacity, 2),
                    "withdrawalCapacity" => (string)round($r->withdrawalCapacity, 2),
                    "status" => $r->status,
                    "trend" => (string)round($r->trend, 2),
                    "full" => (string)round($r->full, 2),
                    "info" => $info,
                ];
                if (empty($r->country_id)) {
                    //add eu/non-eu/ai data
                    $dates[$r->gasDayStart][$r->type] = array_merge($dates[$r->gasDayStart][$r->type], $data);
                } else if (empty($r->company_id)) {
                    //add country data
                    $dates[$r->gasDayStart][$r->type]['children'][$r->country_id] =
                        array_merge(
                            $dates[$r->gasDayStart][$r->type]['children'][$r->country_id],
                            $data
                        );
                    //add consumption/full: <storage>/<consumption>*100
                    //$dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['consumptionFull'] = $r->consumptionFull;
                    try {
                        //add consumption/full: <storage>/<consumption>*100 for EU/NE LEVEL
                        $continent_consumption = (array_key_exists('consumption', $dates[$r->gasDayStart][$r->type])) ?
                            (float)$dates[$r->gasDayStart][$r->type]['consumption'] : 0;
                        $country_consumption = (float)$dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['consumption'];
                        $continent_consumption = $continent_consumption + $country_consumption;
                        $country_consumption_full = '-';
                        try {
                            $gas_in_storage = (float)$dates[$r->gasDayStart][$r->type]['gasInStorage'];
                            $continent_consumption_full = ($gas_in_storage / $continent_consumption) * 100;
                            $country_consumption_full = (round($r->gasInStorage, 4) / $country_consumption) * 100;
                        } catch (Exception $exception) {
                            $continent_consumption_full = 0;
                        }
                        $dates[$r->gasDayStart][$r->type]['consumption'] = '' . round($continent_consumption, 4);
                        $dates[$r->gasDayStart][$r->type]['consumptionFull'] = '' . round($continent_consumption_full, 2);
                        $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['consumptionFull'] = '' . round($country_consumption_full, 2);
                    } catch (Exception $exception) {
                        //dd($dates, $r->gasDayStart, $r, $data, $exception->getLine(), $exception->getMessage());
                        dd($exception->getLine(), $exception->getMessage());
                    }
                } else if (empty($r->facility_id)) {
                    //add company data
                    if (!array_key_exists('children', $dates[$r->gasDayStart][$r->type]['children'][$r->country_id])) {
                        $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'] = [];
                    }
                    if (!array_key_exists($r->company_id, $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'])) {
                        $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'][$r->company_id] = [];
                    }
                    $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'][$r->company_id] = array_merge(
                        $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'][$r->company_id], $data);
                    //add info to higher levels too
                    $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['info'] = array_unique(array_merge(
                            $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['info'],
                            $info)
                    );
                    $dates[$r->gasDayStart][$r->type]['info'] = array_unique(array_merge(
                        $dates[$r->gasDayStart][$r->type]['info'],
                        $info
                    ));
                } else {
                    if (!array_key_exists($r->company_id, $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'])) {
                        $io->error('company not found, changed back when? insert it: '.$r->company_id);
                        //historical changed data? get the company and insert it
                        $year = Carbon::parse($r->gasDayStart)->format('Y');
                        $company_id = $r->company_id;
                        $result2 = $this->doctrine->getRepository(Facility::class)->getListingByCompanyId($year, $company_id);
                        foreach ($result2 as $r2) {
                            $r2 = (object)$r2;
                            if(!is_null($r2->company)) {
                                $dates[$r->gasDayStart][$r2->type]['children'][$r2->country_id]['children'][$r2->company_id] = $data;
                                continue 2;
                            } else {
                                continue 3;
                            }
                        }
                        unset($r2);
                    }
                    if (!array_key_exists('children', $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'][$r->company_id])) {
                        $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'][$r->company_id]['children'] = [];
                    }
                    if (!array_key_exists($r->facility_id, $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'][$r->company_id]['children'])) {
                        $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'][$r->company_id]['children'][$r->facility_id] = [];
                    }
                    $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'][$r->company_id]['children'][$r->facility_id] = array_merge(
                        $dates[$r->gasDayStart][$r->type]['children'][$r->country_id]['children'][$r->company_id]['children'][$r->facility_id],
                        $data);
                }
                unset($r);
                unset($info);
                unset($continent_consumption);
                unset($country_consumption);
                unset($gas_in_storage);
                unset($continent_consumption_full);
            }
        }
    }

    function arrayKeyDates($start, $end = 'now'): array {
        // can use DateTime::createFromFormat() instead
        $startDate = (is_string($start)) ? Carbon::parse($start) : $start;
        $endDate = (is_string($end)) ? Carbon::parse($end) : $end;
        $dates = array();
        if ($startDate === false || is_null($startDate)) {
            // invalid start date.
            return $dates;
        }
        if ($endDate === false || is_null($endDate)) {
            // invalid end date.
            return $dates;
        }
        if ($startDate > $endDate) {
            // start date cannot be greater than end date.
            return $dates;
        }
        try {
            while ($startDate <= $endDate) {
                $dates[$startDate->format('Y-m-d')] = [];
                $startDate->modify('+1 day');
            }
        } catch (Exception $exception) {
            dd($exception->getFile(), $exception->getLine(), $exception->getMessage());
        }
        return $dates;
    }
}