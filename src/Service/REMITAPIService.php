<?php

namespace App\Service;
use Exception;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class REMITAPIService {
    private $params, $url, $key, $client;

    /**
     * REMITAPIService constructor.
     * @param ParameterBagInterface $params
     * @param HttpClientInterface $client
     */
    public function __construct(ParameterBagInterface $params, HttpClientInterface $client) {
        $this->url = $params->get('remit.url');
        $this->key = $params->get('remit.key');
        $this->client = $client;
        $this->params = $params;
    }

    private function getExpiry(): \DateTime {
        $dtz = new \DateTimeZone('Europe/Brussels');
        $now = new \DateTime('now', $dtz);
        //$now->setTime('1', '00');
        $processing_time1 = new \DateTime('now', $dtz);
        $processing_time1->setTime('19', '40');
        $processing_time2 = new \DateTime('now', $dtz);
        $processing_time2->setTime('00', '00');

        //if now < 1 || now > 2 = 1
        if ($now < $processing_time1 || $now > $processing_time2) {
            if ($now > $processing_time1) {
                $processing_time1->add(new \DateInterval('P1D'));
            }
            $processing_time = $processing_time1;
        } else {
            $processing_time = $processing_time2;
        }
        return $processing_time;
    }

    /**
     * @param string|null $params
     * @param bool|null $full
     * @param null $key
     * @return array
     * @throws InvalidArgumentException
     */
    public function getReports(string $params = null, bool $full = null, $key = null): array {
        $cache = new FilesystemAdapter();

        if($key !== $this->key) {
            //if direct api call = do not cache
            return $this->getReportsFromClient($params, $key);
        } else {
            $cache_name = 'reports_sso_';
            self::deleteCache($cache, $cache_name, $params);
            $reports = $cache->get($cache_name . $params, function (ItemInterface $item) use ($cache, $params, $full, $key) {
                $item->expiresAt(self::getExpiry());
                //$item->expiresAfter(3600);

                return $this->getReportsFromClient($params, $key);
            }, 1.0);
            //clear cache if return is empty
            if ($reports['total'] === 0) {
                //total
                $cache->delete($cache_name);
            }
            //let's clear all the cache if there is one with outdated information
            elseif (strpos($params, 'to') === false && $reports['gas_day'] !== $reports['data'][0]['gasDayStart']) {
                $cache->delete($cache_name);
                if(!$cache->hasItem($cache_name)) {
                    return self::getReports($params, $full, $key);
                }
            }
            return $reports;
        }
    }

    private function getReportsFromClient($params, $key): array {
        $url = $this->url . '/far/storage';
        if (!is_null($params)) {
            $url .= '/' . $params;
        }
        //dd($url, $key);
        try {
            $response = $this->client->request(
                'GET',
                $url,
                [
                    'headers' => [
                        'x-key' => $key,
                    ],
                ]
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return $response->toArray();
            } else {
                return [
                    'last_page' => 0,
                    'total' => 0,
                    'dataset' => '',
                    'data' => []
                ];
            }
        } catch (Exception | TransportExceptionInterface $exception) {
            return [
                'last_page' => 0,
                'total' => 0,
                'dataset' => '',
                'error' => 'access denied',
                'message' => $exception->getMessage(). ' ['.$exception->getFile().'/'.$exception->getFile().']',//'Invalid or missing API key',
                'data' => []
            ];
        }
    }

    /**
     * @param string|null $filter
     * @param null $f
     * @param null $key
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getUnavailability(string $filter = null, $f = null, $key = null): array {
        $url = $this->url . '/unr/storage/';
        if (!is_null($filter)) {
            $url .= $filter;
        }
        try {
            $response = $this->client->request(
                'GET',
                $url,
                [
                    'headers' => [
                        'x-key' => $key,
                    ],
                ]
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return $response->toArray();
            } else {
                return [
                    'last_page' => 0,
                    'total' => 0,
                    'error' => 'access denied',
                    'message' => 'Invalid or missing API key',
                    'data' => []
                ];
            }
        } catch (Exception $exception) {
            //dd($url, $exception->getMessage(), $exception->getFile(), $exception->getLine());
            return [
                'last_page' => 0,
                'total' => 0,
                'error' => 'access denied ' . $exception->getCode(),
                'message' => 'Invalid or missing API key',
                'data' => []
            ];
        }
    }

    /**
     * @param string|null $params
     * @param null $f
     * @param null $key
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getServiceAnnouncements(string $params = null, $f = null, $key = null): array {
        $url = $this->url . '/sas';
        if (!is_null($params)) {
            $url .= '/' . $params . '&type=sso';
        } else {
            $url .= '/type=sso';
        }
        $response = $this->client->request(
            'GET',
            $url,
            [
                'headers' => [
                    'x-key' => $key,
                ],
            ]
        //,['verify_peer' => false, 'verify_host' => false]
        );
        $statusCode = $response->getStatusCode();
        //dd($url, $statusCode, $response);
        if ($statusCode === 200) {
            return $response->toArray();
        } else {
            return [
                'error' => 'access denied',
                'message' => 'Invalid or missing API key',
            ];
        }
    }

    public function getRSS(): ?ResponseInterface {
        $url = $this->url . '/rss';
        try {
            return $this->client->request('GET', $url,
                [
                    'headers' => [
                        'x-key' => 'a1388ddc3391689a5ff9774c525a53551',
                    ],
                ]);
        } catch (TransportExceptionInterface $e) {
            return null;
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getCompanies(string $params = null, $f = null, $key = null): array {
        $cache = new FilesystemAdapter();
        $cache_name = 'companies_sso_';
        self::deleteCache($cache, $cache_name, $params);

        // The callable will only be executed on a cache miss.
        return $cache->get($cache_name . $params, function (ItemInterface $item) use ($params, $key) {
            $item->expiresAfter(3600); //1 hour
            //$item->expiresAt(self::getExpiry());
            $url = $this->url . '/companies/sso';
            if (!is_null($params)) {
                $url .= '?' . $params;
            }
            parse_str(strtolower($params), $parameters);
            //dd($url);
            try {
                $response = $this->client->request(
                    'GET',
                    $url,
                    [
                        'headers' => [
                            'x-key' => $key,
                        ],
                    ]
                //,['verify_peer' => false, 'verify_host' => false]
                );
                $statusCode = $response->getStatusCode();
                if ($statusCode === 200) {
                    $array = $response->toArray();
                    if (array_key_exists('listing', $parameters) || in_array('listing', $parameters)) {
                        if (array_key_exists('SSO', $array)) {
                            $sso = $array['SSO'];
                            $new_array = [];
                            foreach ($sso as $a) {
                                $new_array = array_merge($new_array, $a);
                            }
                            ksort($new_array, SORT_REGULAR);
                            return ['SSO' => $new_array];
                        }
                    }
                    return $array;
                } else {
                    return [
                        'SSO' => [],
                        'error' => 'access denied',
                        'message' => 'Invalid or missing API key',
                    ];
                }
            } catch (Exception $exception) {
                var_dump($exception);
                return [
                    'SSO' => [],
                    'error' => 'access denied',
                    'message' => 'Invalid or missing API key',
                ];
            }
        });
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getAggregated(string $params = null, $f = null, $key = null): array {
        $cache = new FilesystemAdapter();
        $cache_name = 'aggregated_sso_';
        self::deleteCache($cache, $cache_name, $params);

        // The callable will only be executed on a cache miss.
        return $cache->get($cache_name . $params, function (ItemInterface $item) use ($params, $key) {
            $item->expiresAfter(3600); //1 hour
            //$item->expiresAt(self::getExpiry());
            $url = $this->url . '/companies/sso/aggregated';
            if (!is_null($params)) {
                $url .= '?' . $params;
            }
            parse_str(strtolower($params), $parameters);
            //dd($url);
            try {
                $response = $this->client->request(
                    'GET',
                    $url,
                    [
                        'headers' => [
                            'x-key' => $key,
                        ],
                    ]
                //,['verify_peer' => false, 'verify_host' => false]
                );
                $statusCode = $response->getStatusCode();
                if ($statusCode === 200) {
                    return $response->toArray();
                } else {
                    return [
                        'data' => [],
                        'error' => 'access denied',
                        'message' => 'Invalid or missing API key',
                    ];
                }
            } catch (Exception $exception) {
                var_dump($exception);
                return [
                    'data' => [],
                    'error' => 'access denied',
                    'message' => 'Invalid or missing API key',
                ];
            }
        });
    }

    public function login($request): array {
        try {
            $content = (array)json_decode($request->getContent(), true);
            $url = $this->url . '/login';
            //$url = $this->url . '/login';
            $response = $this->client->request(
                'POST',
                $url,
                [
                    'body' => [
                        'web' => 'agsi',
                        'email' => $content['login_email'],
                        'password' => $content['login_password'],
                    ],
                    //'body' => $content,
                ]
            );
            $statusCode = $response->getStatusCode();
            $content = json_decode($response->getContent());
            if ($statusCode === 200) {
                try {
                    return (array)json_decode($response->getContent());
                } catch (\Exception $exception) {
                    if ($content->email) {
                        return ['error' => $content->email];
                    } else if ($content->password) {
                        return ['error' => $content->password];
                    }
                }
            } else {
                return self::somethingWentWrong($statusCode);
            }
        } catch (Exception $exception) {
            return self::somethingWentWrong($exception->getMessage() . ' / ' . $exception->getFile() . ' - ' . $exception->getLine());
        } catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface$e) {
            return (array)$e;
        }
    }

    public function forgot($request): array {
        try {
            $content = (array)json_decode($request->getContent(), true);
            $url = $this->url . '/forgot';
            //$url = str_replace('remit', 'remittest', $url);
            $response = $this->client->request(
                'POST',
                $url,
                [
                    'body' => [
                        'web' => 'agsi',
                        'email' => $content['login_email'],
                    ],
                    //'body' => $content,
                ]
            );
            $statusCode = $response->getStatusCode();
            $content = json_decode($response->getContent());
            if ($statusCode === 200) {
                try {
                    return (array)json_decode($response->getContent());
                } catch (\Exception $exception) {
                    if ($content->email) {
                        return ['error' => $content->email];
                    }
                }
            } else {
                return self::somethingWentWrong($statusCode);
            }
        } catch (Exception $exception) {
            return self::somethingWentWrong($exception->getMessage() . ' / ' . $exception->getFile() . ' - ' . $exception->getLine());
        } catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface$exception) {
            return (array)$exception;
        }
    }

    public function register($request): array {
        try {
            $content = (array)json_decode($request->getContent(), true);
            $body = [
                'web' => 'agsi',
            ];
            foreach($content as $key => $value) {
                $body = array_merge($body, [str_replace('register_', '', $key) => $value]);
            }
            $body['slug'] = $body['first_name'].' '.$body['last_name'];
            //$url = str_replace(['remit', 'api'], ['remittest', 'auth'], $this->url) . '/register';
            $url = str_replace('api', 'auth', $this->url) . '/register';
            $response = $this->client->request(
                'POST',
                $url,
                [
                    'body' => $body,
                ]
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return (array)json_decode($response->getContent());
            } else {
                return self::somethingWentWrong($statusCode);
            }
        } catch (Exception $exception) {
            return self::somethingWentWrong($exception->getMessage() . ' / ' . $exception->getFile() . ' - ' . $exception->getLine());
        } catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface$e) {
            return (array)$e;
        }
    }

    public function update($request): array {
        try {
            $content = (array)json_decode($request->getContent(), true);
            $body = [
                'web' => 'agsi',
            ];
            foreach($content as $item) {
                $body = array_merge($body, [$item['name'] => $item['value']]);
            }
            $body['slug'] = $body['first_name'].' '.$body['last_name'];
            $url = str_replace('api', 'auth', $this->url) . '/update';
            $response = $this->client->request(
                'POST',
                $url,
                [
                    'body' => $body,
                    //'body' => $content,
                ]
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return (array)json_decode($response->getContent());
            } else {
                return self::somethingWentWrong($url . ' - ' . $statusCode);
            }
        } catch (Exception $exception) {
            return self::somethingWentWrong($exception->getMessage() . ' / ' . $exception->getFile() . ' - ' . $exception->getLine());
        } catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface$e) {
            return (array)$e;
        }
    }

    public function delete($request): array {
        try {
            $content = (array)json_decode($request->getContent(), true);
            $body = [
                'web' => 'agsi',
            ];
            foreach($content as $item) {
                $body = array_merge($body, [$item['name'] => $item['value']]);
            }
            $body['slug'] = $body['first_name'].' '.$body['last_name'];
            //$url = str_replace(['remit', 'api'], ['remittest', 'auth'], $this->url) . '/delete';
            $url = str_replace('api', 'auth', $this->url) . '/delete';
            $response = $this->client->request(
                'POST',
                $url,
                [
                    'body' => $body,
                    //'body' => $content,
                ]
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return (array)json_decode($response->getContent());
            } else {
                return self::somethingWentWrong($url . ' - ' . $statusCode);
            }
        } catch (Exception $exception) {
            return self::somethingWentWrong($exception->getMessage() . ' / ' . $exception->getFile() . ' - ' . $exception->getLine());
        } catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface$e) {
            return (array)$e;
        }
    }

    private function somethingWentWrong($extra = ''): array {
        return [
            'error' => '500',
            'message' => 'Something went wrong. If it continues, please contact api@gie.eu for assistance',
            'info' => $extra
        ];
    }

    private function deleteCache($cache, $cache_name, &$params) {
        $params = strtolower($params);
        if(strpos('deletethecache', $params) >= 0) {
            $params = str_replace('deletethecache', '', $params);
            $cache->delete($cache_name . $params);
        }
    }
}