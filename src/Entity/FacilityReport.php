<?php

namespace App\Entity;

use App\Repository\FacilityReportRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacilityReportRepository::class)
 * @ORM\Table(name="report_facility_storage")
 */
class FacilityReport {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $updated_at;
    /**
     * @ORM\Column(type="date")
     */
    private $gas_day_start;
    /**
     * @ORM\Column(type="date")
     */
    private $gas_day_end;
    /**
     * @ORM\Column(type="decimal", precision=20, scale=15)
     */
    private $storage_value;
    /**
     * @ORM\Column(type="decimal", precision=20, scale=15)
     */
    private $injection_value;
    /**
     * @ORM\Column(type="decimal", precision=20, scale=15)
     */
    private $withdrawal_value;
    /**
     * @ORM\Column(type="decimal", precision=20, scale=15)
     */
    private $technical_capacity_value;
    /**
     * @ORM\Column(type="decimal", precision=20, scale=15)
     */
    private $contracted_capacity_value;
    /**
     * @ORM\Column(type="decimal", precision=20, scale=15)
     */
    private $available_capacity_value;
    /**
     * @ORM\Column(type="decimal", precision=20, scale=15)
     */
    private $dtmti_value;
    /**
     * @ORM\Column(type="decimal", precision=20, scale=15)
     */
    private $dtmtw_value;
    /**
     * @ORM\Column(type="smallint")
     */
    private $estimated;
    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="facilityReports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $facility_operator_identifier_id;
    /**
     * @ORM\ManyToOne(targetEntity=Facility::class, inversedBy="facilityReports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $facility_identifier_id;
    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $notifications;

    public function getId(): ?int {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeImmutable {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeImmutable $updated_at): self {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function getGasDayStart(): ?\DateTimeInterface {
        return $this->gas_day_start;
    }

    public function setGasDayStart(\DateTimeInterface $gas_day_start): self {
        $this->gas_day_start = $gas_day_start;
        return $this;
    }

    public function getGasDayEnd(): ?\DateTimeInterface {
        return $this->gas_day_end;
    }

    public function setGasDayEnd(\DateTimeInterface $gas_day_end): self {
        $this->gas_day_end = $gas_day_end;
        return $this;
    }

    public function getStorageValue(): ?string {
        return $this->storage_value;
    }

    public function setStorageValue(string $storage_value): self {
        $this->storage_value = $storage_value;
        return $this;
    }

    public function getInjectionValue(): ?string {
        return $this->injection_value;
    }

    public function setInjectionValue(string $injection_value): self {
        $this->injection_value = $injection_value;
        return $this;
    }

    public function getWithdrawalValue(): ?string {
        return $this->withdrawal_value;
    }

    public function setWithdrawalValue(string $withdrawal_value): self {
        $this->withdrawal_value = $withdrawal_value;
        return $this;
    }

    public function getTechnicalCapacityValue(): ?string {
        return $this->technical_capacity_value;
    }

    public function setTechnicalCapacityValue(string $technical_capacity_value): self {
        $this->technical_capacity_value = $technical_capacity_value;
        return $this;
    }

    public function getContractedCapacityValue(): ?string {
        return $this->contracted_capacity_value;
    }

    public function setContractedCapacityValue(string $contracted_capacity_value): self {
        $this->contracted_capacity_value = $contracted_capacity_value;
        return $this;
    }

    public function getAvailableCapacityValue(): ?string {
        return $this->available_capacity_value;
    }

    public function setAvailableCapacityValue(string $available_capacity_value): self {
        $this->available_capacity_value = $available_capacity_value;
        return $this;
    }

    public function getDTMTIValue(): ?string {
        return $this->dtmti_value;
    }

    public function setDTMTIValue(string $dtmti_value): self {
        $this->dtmti_value = $dtmti_value;
        return $this;
    }

    public function getDTMTWValue(): ?string {
        return $this->dtmtw_value;
    }

    public function setDTMTWValue(string $dtmtw_value): self {
        $this->dtmtw_value = $dtmtw_value;
        return $this;
    }

    public function getEstimated(): ?int {
        return $this->estimated;
    }

    public function setEstimated(int $estimated): self {
        $this->estimated = $estimated;
        return $this;
    }

    public function getFacilityOperatorIdentifierId(): ?Company {
        return $this->facility_operator_identifier_id;
    }

    public function setFacilityOperatorIdentifierId(?Company $facility_operator_identifier_id): self {
        $this->facility_operator_identifier_id = $facility_operator_identifier_id;
        return $this;
    }

    public function getFacilityIdentifierId(): ?Facility {
        return $this->facility_identifier_id;
    }

    public function setFacilityIdentifierId(?Facility $facility_identifier_id): self {
        $this->facility_identifier_id = $facility_identifier_id;
        return $this;
    }

    public function getNotifications(): ?string {
        return $this->notifications;
    }

    public function setNotifications(?string $notifications): self {
        $this->notifications = $notifications;
        return $this;
    }
}