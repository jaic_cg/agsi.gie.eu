<?php

namespace App\Entity;

use App\Repository\EICRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EICRepository::class)
 */
class EIC {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity=Facility::class, inversedBy="eics")
     * @ORM\JoinColumn(nullable=false)
     */
    private $facility_id;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $eic;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $eic_display_name;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $tag;

    public function getId(): ?int {
        return $this->id;
    }

    public function getFacilityId(): ?Facility {
        return $this->facility_id;
    }

    public function setFacilityId(?Facility $facility_id): self {
        $this->facility_id = $facility_id;
        return $this;
    }

    public function getEic(): ?string {
        return $this->eic;
    }

    public function setEic(string $eic): self {
        $this->eic = $eic;
        return $this;
    }

    public function getEicDisplayName(): ?string {
        return $this->eic_display_name;
    }

    public function setEicDisplayName(string $eic_display_name): self {
        $this->eic_display_name = $eic_display_name;
        return $this;
    }

    public function getTag(): ?string {
        return $this->tag;
    }

    public function setTag(?string $tag): self {
        $this->tag = $tag;
        return $this;
    }
}
