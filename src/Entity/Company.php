<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 */
class Company {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="companies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $country_id;
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $updated_at;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $short_name;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $eic_display_name;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $eic;
    /**
     * @ORM\Column(type="integer")
     */
    private $type;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $publication_link;
    /**
     * @ORM\Column(type="integer")
     */
    private $status;
    /**
     * @ORM\Column(type="smallint")
     */
    private $send_acer;
    /**
     * @ORM\Column(type="smallint")
     */
    private $send_umm;
    /**
     * @ORM\Column(type="integer")
     */
    private $webstatus;
    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="children")
     */
    private $proxy_id;
    /**
     * @ORM\OneToMany(targetEntity=Company::class, mappedBy="proxy_id")
     */
    private $children;
    /**
     * @ORM\OneToMany(targetEntity=Facility::class, mappedBy="company_id")
     */
    private $facilities;

    /**
     * @ORM\OneToMany(targetEntity=FacilityReport::class, mappedBy="facility_operator_identifier_id")
     */
    private $facilityReports;

    public function __construct() {
        $this->children = new ArrayCollection();
        $this->facilities = new ArrayCollection();
        $this->facilityReports = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCountryId(): ?Country {
        return $this->country_id;
    }

    public function setCountryId(?Country $country_id): self {
        $this->country_id = $country_id;
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeImmutable $updated_at): self {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function getSlug(): ?string {
        return $this->slug;
    }

    public function setSlug(string $slug): self {
        $this->slug = $slug;
        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    public function getShortName(): ?string {
        return $this->short_name;
    }

    public function setShortName(string $short_name): self {
        $this->short_name = $short_name;
        return $this;
    }

    public function getEicDisplayName(): ?string {
        return $this->eic_display_name;
    }

    public function setEicDisplayName(string $eic_display_name): self {
        $this->eic_display_name = $eic_display_name;
        return $this;
    }

    public function getEic(): ?string {
        return $this->eic;
    }

    public function setEic(string $eic): self {
        $this->eic = $eic;
        return $this;
    }

    public function getType(): ?int {
        return $this->type;
    }

    public function setType(int $type): self {
        $this->type = $type;
        return $this;
    }

    public function getImage(): ?string {
        return $this->image;
    }

    public function setImage(?string $image): self {
        $this->image = $image;
        return $this;
    }

    public function getPublicationLink(): ?string {
        return $this->publication_link;
    }

    public function setPublicationLink(?string $publication_link): self {
        $this->publication_link = $publication_link;
        return $this;
    }

    public function getStatus(): ?int {
        return $this->status;
    }

    public function setStatus(int $status): self {
        $this->status = $status;
        return $this;
    }

    public function getSendAcer(): ?int {
        return $this->send_acer;
    }

    public function setSendAcer(int $send_acer): self {
        $this->send_acer = $send_acer;
        return $this;
    }

    public function getSendUmm(): ?int {
        return $this->send_umm;
    }

    public function setSendUmm(int $send_umm): self {
        $this->send_umm = $send_umm;
        return $this;
    }

    public function getWebstatus(): ?int {
        return $this->webstatus;
    }

    public function setWebstatus(int $webstatus): self {
        $this->webstatus = $webstatus;
        return $this;
    }

    public function getProxyId(): ?self {
        return $this->proxy_id;
    }

    public function setProxyId(?self $proxy_id): self {
        $this->proxy_id = $proxy_id;
        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getChildren(): Collection {
        return $this->children;
    }

    public function addChild(self $child): self {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setProxyId($this);
        }
        return $this;
    }

    public function removeChild(self $child): self {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getProxyId() === $this) {
                $child->setProxyId(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Facility>
     */
    public function getFacilities(): Collection {
        return $this->facilities;
    }

    public function addFacility(Facility $facility): self {
        if (!$this->facilities->contains($facility)) {
            $this->facilities[] = $facility;
            $facility->setCompanyId($this);
        }
        return $this;
    }

    public function removeFacility(Facility $facility): self {
        if ($this->facilities->removeElement($facility)) {
            // set the owning side to null (unless already changed)
            if ($facility->getCompanyId() === $this) {
                $facility->setCompanyId(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, FacilityReport>
     */
    public function getFacilityReports(): Collection
    {
        return $this->facilityReports;
    }

    public function addFacilityReports(FacilityReport $facilityReport): self
    {
        if (!$this->facilityReports->contains($facilityReport)) {
            $this->facilityReports[] = $facilityReport;
            $facilityReport->setFacilityOperatorIdentifierId($this);
        }

        return $this;
    }

    public function removeFacilityReport(FacilityReport $facilityReport): self
    {
        if ($this->facilityReports->removeElement($facilityReport)) {
            // set the owning side to null (unless already changed)
            if ($facilityReport->getFacilityOperatorIdentifierId() === $this) {
                $facilityReport->setFacilityOperatorIdentifierId(null);
            }
        }

        return $this;
    }
}