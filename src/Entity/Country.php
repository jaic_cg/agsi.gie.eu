<?php

namespace App\Entity;

use App\Repository\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 */
class Country {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=10)
     */
    private $code;
    /**
     * @ORM\Column(type="smallint")
     */
    private $type;
    /**
     * @ORM\OneToMany(targetEntity=Company::class, mappedBy="country_id")
     */
    private $companies;
    /**
     * @ORM\OneToMany(targetEntity=Facility::class, mappedBy="country_id")
     */
    private $facilities;

    public function __construct() {
        $this->companies = new ArrayCollection();
        $this->facilities = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getSlug(): ?string {
        return $this->slug;
    }

    public function setSlug(string $slug): self {
        $this->slug = $slug;
        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    public function getCode(): ?string {
        return $this->code;
    }

    public function setCode(string $code): self {
        $this->code = $code;
        return $this;
    }

    public function getType(): ?int {
        return $this->type;
    }

    public function setType(int $type): self {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Collection<int, Company>
     */
    public function getCompanies(): Collection {
        return $this->companies;
    }

    public function addCompany(Company $company): self {
        if (!$this->companies->contains($company)) {
            $this->companies[] = $company;
            $company->setCountryId($this);
        }
        return $this;
    }

    public function removeCompany(Company $company): self {
        if ($this->companies->removeElement($company)) {
            // set the owning side to null (unless already changed)
            if ($company->getCountryId() === $this) {
                $company->setCountryId(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Facility>
     */
    public function getFacilities(): Collection {
        return $this->facilities;
    }

    public function addFacility(Facility $facility): self {
        if (!$this->facilities->contains($facility)) {
            $this->facilities[] = $facility;
            $facility->setCountryId($this);
        }
        return $this;
    }

    public function removeFacility(Facility $facility): self {
        if ($this->facilities->removeElement($facility)) {
            // set the owning side to null (unless already changed)
            if ($facility->getCountryId() === $this) {
                $facility->setCountryId(null);
            }
        }
        return $this;
    }
}