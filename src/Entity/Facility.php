<?php

namespace App\Entity;

use App\Repository\FacilityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacilityRepository::class)
 */
class Facility {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="facilities")
     * @ORM\JoinColumn(nullable=false)
     */
    private $country_id;
    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="facilities")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company_id;
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $updated_at;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    /**
     * @ORM\Column(type="integer")
     */
    private $type;
    /**
     * @ORM\Column(type="integer")
     */
    private $status;
    /**
     * @ORM\Column(type="integer")
     */
    private $web_status;
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $start_date;
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $stop_date;
    /**
     * @ORM\Column(type="string", length=20)
     */
    private $timezone;
    /**
     * @ORM\Column(type="smallint")
     */
    private $mail_unreceived_far;
    /**
     * @ORM\OneToMany(targetEntity=EIC::class, mappedBy="facility_id", orphanRemoval=true)
     */
    private $eics;

    /**
     * @ORM\OneToMany(targetEntity=FacilityReport::class, mappedBy="facility_identifier_id")
     */
    private $facilityReports;

    public function __construct() {
        $this->eics = new ArrayCollection();
        $this->facilityReports = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCountryId(): ?Country {
        return $this->country_id;
    }

    public function setCountryId(?Country $country_id): self {
        $this->country_id = $country_id;
        return $this;
    }

    public function getCompanyId(): ?Company {
        return $this->company_id;
    }

    public function setCompanyId(?Company $company_id): self {
        $this->company_id = $company_id;
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeImmutable $updated_at): self {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function getSlug(): ?string {
        return $this->slug;
    }

    public function setSlug(string $slug): self {
        $this->slug = $slug;
        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    public function getType(): ?int {
        return $this->type;
    }

    public function setType(int $type): self {
        $this->type = $type;
        return $this;
    }

    public function getStatus(): ?int {
        return $this->status;
    }

    public function setStatus(int $status): self {
        $this->status = $status;
        return $this;
    }

    public function getWebStatus(): ?string {
        return $this->web_status;
    }

    public function setWebStatus(string $web_status): self {
        $this->web_status = $web_status;
        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface {
        return $this->start_date;
    }

    public function setStartDate(?\DateTimeInterface $start_date): self {
        $this->start_date = $start_date;
        return $this;
    }

    public function getStopDate(): ?\DateTimeInterface {
        return $this->stop_date;
    }

    public function setStopDate(?\DateTimeInterface $stop_date): self {
        $this->stop_date = $stop_date;
        return $this;
    }

    public function getTimezone(): ?string {
        return $this->timezone;
    }

    public function setTimezone(string $timezone): self {
        $this->timezone = $timezone;
        return $this;
    }

    public function getMailUnreceivedFar(): ?int {
        return $this->mail_unreceived_far;
    }

    public function setMailUnreceivedFar(int $mail_unreceived_far): self {
        $this->mail_unreceived_far = $mail_unreceived_far;
        return $this;
    }

    /**
     * @return Collection<int, EIC>
     */
    public function getEics(): Collection {
        return $this->eics;
    }

    public function addEic(EIC $eic): self {
        if (!$this->eics->contains($eic)) {
            $this->eics[] = $eic;
            $eic->setFacilityId($this);
        }
        return $this;
    }

    public function removeEic(EIC $eic): self {
        if ($this->eics->removeElement($eic)) {
            // set the owning side to null (unless already changed)
            if ($eic->getFacilityId() === $this) {
                $eic->setFacilityId(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, FacilityReport>
     */
    public function getFacilityReports(): Collection
    {
        return $this->facilityReports;
    }

    public function addFacilityReport(FacilityReport $facilityReport): self
    {
        if (!$this->facilityReports->contains($facilityReport)) {
            $this->facilityReports[] = $facilityReport;
            $facilityReport->setFacilityIdentifierId($this);
        }

        return $this;
    }

    public function removeFacilityReport(FacilityReport $facilityReport): self
    {
        if ($this->facilityReports->removeElement($facilityReport)) {
            // set the owning side to null (unless already changed)
            if ($facilityReport->getFacilityIdentifierId() === $this) {
                $facilityReport->setFacilityIdentifierId(null);
            }
        }

        return $this;
    }
}