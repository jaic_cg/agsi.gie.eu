<?php

namespace App\Controller;

use App\Service\REMITAPIService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class APIController extends AbstractController {
    private $REMITAPIService;

    public function __construct(REMITAPIService $REMITAPIService) {
        $this->REMITAPIService = $REMITAPIService;
    }

    /**
     * @Route("/api/data/{param1}/{param2?}/{param3?}")
     */
    public function oldApi(Request $request, string $param1, string $param2 = null, string $param3 = null): Response {
        $params = self::convertOldParams($request->getQueryString());
        if (strtolower($param1) === 'tree') {
            $link = 'date=' . date("Y-m-d");
        } else if (in_array(strtolower($param1), ['eu', 'ne', 'ai'])) {
            $link = 'continent=' . $param1;
        } else {
            if (!is_null($param3)) {
                //facility/country/company
                $link = 'country=' . $param2 . '&company=' . $param3 . '&facility=' . $param1;
            } else if (!is_null($param2)) {
                //company/country
                $link = 'country=' . $param2 . '&company=' . $param1;
            } else {
                //country
                $link = 'country=' . $param1;
            }
        }
        $return = $this->api($request, 'reports', $link . $params);
        try {
            return $this->json($return);
        } catch(\Exception $exception) {
            return [
                'last_page' => 0,
                'total' => 0,
                'error' => 'access denied',
                'message' => 'Invalid or missing API key',
                'data' => []
            ];
        }
    }

    /**
     * @Route("/api/countries/type/SSO/companies/full")
     */
    public function oldSSOAPI(): Response {
        return $this->json($this->api(new Request(), 'about', 'show=listing'));
    }

    private function convertOldParams($params): ?string {
        return (empty($params)) ? null : '&' . str_replace(
                ['from', 'till', 'limit', 'type'],
                ['from', 'to', 'size', 'continent'],
                $params
            );
    }

    /**
     * @Route("/api/login", name="post_login", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function login(Request $request) {
        $session = $request->getSession();
        $return = $this->REMITAPIService->login($request);
        if(array_key_exists('access_token', $return)) {
            // stores an attribute in the session for later reuse
            $session->set('is_logged_in', $return);
            return $this->json(['success' => 'logged in']);
        } else {
            return $this->json($return);
        }
    }

    /**
     * @Route("/api/forgot", name="post_forgot", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function forgot(Request $request) {
        $return = $this->REMITAPIService->forgot($request);
        return (is_array($return)) ?
            $this->json($return) :
            $return;
    }

    /**
     * @Route("/api/register", name="post_register", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function register(Request $request) {
        $session = $request->getSession();
        $return = $this->REMITAPIService->register($request);
        if(array_key_exists('access_token', $return)) {
            // stores an attribute in the session for later reuse
            $session->set('is_logged_in', $return);
            return $this->json(['success' => 'logged in']);
        } else {
            return $this->json($return);
        }
    }

    /**
     * @Route("/api/update", name="post_update", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function update(Request $request) {
        $session = $request->getSession();
        if($session->get('is_logged_in')) {
            $return = $this->REMITAPIService->update($request);
            if (array_key_exists('api_access', $return)) {
                // stores an attribute in the session for later reuse
                $return = (array_replace($session->get('is_logged_in'), $return));
                $session->set('is_logged_in', $return);
                return $this->json(['success' => 'updated']);
            } else {
                return $this->json($return);
            }
        } else {
            return $this->json(['error' => 'something went wrong']);
        }
    }

    /**
     * @Route("/api/delete", name="post_delete", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function delete(Request $request) {
        $session = $request->getSession();
        $return = $this->REMITAPIService->delete($request);
        if(array_key_exists('deleted', $return)) {
            $session->clear();
            return $this->json(['success' => 'account removed']);
        } else {
            return $this->json($return);
        }
    }

    /**
     * @Route("/api/logout", name="post_logout", methods={"POST"})
     * @param Request $request
     * @return array|JsonResponse
     */
    public function logout(Request $request) {
        $session = $request->getSession();
        $session->clear();
        return $this->json(['success' => 'logged out']);
    }

    /**
     * @Route("/api/letusclearthecache", name="api_clearcaches")
     */
    public function clearCaches() {
        $cache = new FilesystemAdapter();
        $cache->clear();
        return $this->json(['cache cleared']);
    }

    /**
     * @Route("/api/{action}", name="api")
     * @param Request $request
     * @param string $action
     * @param null $oldAPIParams
     * @return array|JsonResponse
     */
    public function api(Request $request, string $action = 'reports', $oldAPIParams = null) {
        $oldAPI = false;
        try {
            $headers = $request->headers;
            if(
                in_array($headers->get('sec-fetch-mode'), ['navigate', 'cors'])
                ||
                strpos(strtolower($headers->get('user-agent')), 'mac') !== false
            ) {
                $key = 'a1388ddc3391689a5ff9774c525a53551';
            } else {
                $key = $headers->get('x-key');
            }
            //$params = [];
            //parse_str($request->getQueryString(), $params);
            $params = $request->getQueryString();
            if (!is_null($oldAPIParams)) {
                $params = $oldAPIParams;
                $oldAPI = true;
            }
            if(strlen($params) === 0) {
                $params = 'date=' . date("Y-m-d");
            }
            switch ($action) {
                case 'news':
                    $function = 'getServiceAnnouncements';
                    break;
                case 'about':
                    $function = 'getCompanies';
                    break;
                case 'about-aggregated':
                    $function = 'getAggregated';
                    break;
                case 'unavailability':
                    $function = 'getUnavailability';
                    break;
                case 'reports':
                default:
                    $params = self::convertOldParams($params);
                    $function = 'getReports';
            }
            try {
                $return = $this->REMITAPIService->$function($params, false, $key);
            } catch (ClientExceptionInterface $e) {
                $return = ['type' => 'ClientException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
            } catch (DecodingExceptionInterface $e) {
                $return = ['type' => 'DecodingException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
            } catch (RedirectionExceptionInterface $e) {
                $return = ['type' => 'RedirectionException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
            } catch (ServerExceptionInterface $e) {
                $return = ['type' => 'ServerException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
            } catch (TransportExceptionInterface $e) {
                $return = ['type' => 'TransportException', 'code' => $e->getCode(), 'message' => $e->getMessage()];
            }
            if (strpos($params, 'extra=graphs') !== false) {
                $data = $return['data'];
                $new_data = [];
                switch ($action) {
                    case 'unavailability':
                        break;
                    case 'reports':
                    default:
                        $i = -1;
                        $fields = [
                            //value => group
                            'gasInStorage' => 'Storage',
                            'workingGasVolume' => 'Working (gas) volume',
                            'injection' => 'Injection',
                            'injectionCapacity' => 'Injection capacity',
                            'withdrawal' => 'Withdrawal',
                            'withdrawalCapacity' => 'Withdrawal capacity',
                        ];
                        foreach ($data as $d) {
                            $i++;
                            $new_data[$i]['gasDayStart'] = $d['gasDayStart'];
                            foreach ($fields as $value => $group) {
                                try {
                                    $new_data[$i][$value] = str_replace(',', '.', $d[$value]) + 0;
                                } catch (\Exception $exception) {
                                    //dd($exception->getMessage(), $d['gasDayStart'], $d[$value]);
                                    continue;
                                    $new_data[$i][$value] = null;
                                }
                            }
                            if(count($new_data[$i]) === 1) {
                                unset($new_data[$i]);
                            }
                            /*
                            foreach ($fields as $value => $group) {
                                $i++;
                                $new_data[$i]['group'] = $group;//dataset 1: gasInStorage
                                $new_data[$i]['key'] = $d['gasDayStart'];
                                $new_data[$i]['value'] = str_replace(',', '.', $d[$value]) + 0;
                            }
                            */
                        }
                        //$reversed = array_reverse($new_data);
                        $return['pointInterval'] = '86400000';
                        $return['pointStart'] = '' . 1000 * strtotime((count($new_data) > 0) ? $new_data[count($new_data) - 1]['gasDayStart'] : date('Y-m-d'));
                        $return['data'] = $new_data;
                }
                if (strpos($params, 'show=listing') !== false) {
                    $new_data = [];
                    foreach ($return as $type => $t_data) {
                        foreach ($t_data as $continent => $c_data) {
                            foreach ($c_data as $country => $companies) {
                                foreach ($companies as $company) {
                                    $new_data[] = [
                                        'name' => $company['name'],
                                        'short_name' => $company['short_name'],
                                        'type' => $company['data']['type'],
                                        'eic' => $company['eic'],
                                        'country_slug' => $company['data']['country']['code'],
                                        'country' => $company['data']['country']['name']
                                    ];
                                    foreach ($company['facilities'] as $facility) {
                                        $new_data[] = [
                                            'name' => $facility['name'],
                                            'short_name' => $facility['name'],
                                            'type' => $facility['type'],
                                            'eic' => $facility['eic'],
                                            'country_slug' => $company['data']['country']['code'],
                                            'country' => $company['data']['country']['name']
                                        ];
                                    }
                                }
                            }
                        }
                    }
                    $return = $new_data;
                }
            }
        } catch (\Exception $exception) {
            $exception = new AccessDeniedException('Access Denied. You need a (valid) API key');
            $return = ['code' => $exception->getCode(), 'message' => $exception->getMessage()];
            //$return = $exception;
        }
        return ($oldAPI) ? $return : $this->json($return);
    }
}