<?php

namespace App\Controller;

use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Asset\Packages;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController {
    private $requestStack;
    private $manager;

    public function __construct(RequestStack $requestStack, Packages $manager) {
        $this->requestStack = $requestStack;
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(): Response {
        return $this->render('index.html.twig', [
            'svelteId' => 'main',
            'params' => null,
        ]);
    }

    /**
     * @Route("/test/dashboard", name="test_dashboard")
     * @return Response
     */
    public function dashboard(): Response {
	    $marker_planned = $this->manager->getUrl('build/img/marker-icon-planned.png');
	    $marker_unplanned = $this->manager->getUrl('build/img/marker-icon-unplanned.png');

        return $this->render('dashboard.html.twig', [
            'title' => 'Dashboard',
            'svelteId' => 'dashboard',
            'params' => '?continent=eu&extra=graphs',
	        'icons' => $marker_planned . '|' . $marker_unplanned
        ]);
    }

    /**
     * @Route("/data-overview/{code1}/{code2}/{code3}/{code4}", name="data_overview")
     * @param string $code1
     * @param string|null $code2
     * @param string|null $code3
     * @param string|null $code4
     * @return Response
     */
    public function historical(string $code1, string $code2 = null, string $code3 = null, string $code4 = null): Response {
        if($code1 === 'graphs') {
            $params = DefaultController::makeFilter($code2, $code3, $code4);//.'&size=1000'
            return $this->render('graphs.html.twig', [
                'title' => 'Data Overview / Graphs',
                'svelteId' => 'graphs',
                'params' => "?" . $params,
            ]);
        }
        return $this->render('historical.html.twig', [
            'title' => 'Data Overview / Historical Data',
            'svelteId' => 'historical',
            'params' => "?" . DefaultController::makeFilter($code1, $code2, $code3),
        ]);
    }

    /**
     * @Route("/graphs/{code1}/{code2}/{code3}", name="graphs")
     * @param string $code1
     * @param string|null $code2
     * @param string|null $code3
     * @return Response
     */
    public function graphs(string $code1, string $code2 = null, string $code3 = null): Response {
        return $this->render('graphs.html.twig', [
            'svelteId' => 'graphs',
            'title' => 'Data Overview / Graphs',
            'params' => "?" . DefaultController::makeFilter($code1, $code2, $code3),//.'&size=1000',
        ]);
    }

    /**
     * @Route("/capacity-forecast", name="capacity_forecast")
     */
    public function capacityForecast(): Response {
        return $this->render('capacityForecast.html.twig', [
            'svelteId' => 'capacityForecast',
            'title' => 'Capacity Forecast',
            'params' => '?listing=true&hide=capacity',
        ]);
    }

    /**
     * @Route("/unavailability/{code1}/{code2}/{code3}/{code4}", name="unavailability")
     */
    public function unavailabilityReports(string $code1 = null, string $code2 = null, string $code3 = null, string $code4 = null): Response {
        if($code1 === 'gantt') {
	        if($code2 === null || $code2 === 'undefined') {
		        $code2 = 'eu';
	        }
	        if(in_array(strtolower($code2), ['eu', 'ne', 'ai'])) {
		        $redirect_keys = [
			        'eu' => 'at',
			        'ai' => 'es*',
			        'ne' => 'gb*',
		        ];
		        $key = $redirect_keys[strtolower($code2)];
		        //nothing selected, redirect to AT
		        return $this->redirectToRoute('unavailability', ['code1' => 'gantt', 'code2' => $key]);
	        }
	        $params = DefaultController::makeFilter($code2, $code3, $code4);//.'&size=1000'
	        return $this->render('gantt.html.twig', [
		        'currentPageTitle' => 'Unavailability / Timeline',
		        'svelteId' => 'gantt',
		        'params' => "?" . $params,
	        ]);
        } elseif($code1 === 'map') {
            if($code2 === null) {
                //nothing selected, redirect to AT
                $code2 = Carbon::now()->format('Y-m-d');
            }
            $params = DefaultController::makeFilter($code2, $code3, $code4);//.'&size=1000'


            $marker_planned = $this->manager->getUrl('build/img/marker-icon-planned.png');
            $marker_unplanned = $this->manager->getUrl('build/img/marker-icon-unplanned.png');
            return $this->render('unavailabilityMap.html.twig', [
                'currentPageTitle' => 'Unavailability / Map',
                'svelteId' => 'unavailability_map',
                'params' => "?" . $params,
                'icons' => $marker_planned . '|' . $marker_unplanned
            ]);
        }
        return $this->render('unavailability.html.twig', [
            'currentPageTitle' => 'Unavailability',
            'svelteId' => 'unavailability',
            'params' => "?" . DefaultController::makeFilter($code1, $code2, $code3),
        ]);
    }
/*
    /**
     * @Route("/gantt/{code1}/{code2}/{code3}", name="gantt")
     *
    public function gantt(string $code1 = null, string $code2 = null, string $code3 = null): Response {
        return $this->render('gantt.html.twig', [
            'svelteId' => 'gantt',
            'params' => "?" . DefaultController::makeFilter($code1, $code2, $code3, true),
        ]);
    }
*/
    /**
     * @Route("/news/{messageId?}", name="news")
     */
    public function news(string $messageId = null): Response {
        $params = (!is_null($messageId)) ? '?url=' . $messageId : '';
        return $this->render('news.html.twig', [
            'svelteId' => 'news',
            'params' => $params,
        ]);
    }

    /**
     * @Route("/data-definition", name="data_definition")
     */
    public function dataDefinition(): Response {
        return $this->render('dataDefinitions.html.twig', [
            'svelteId' => 'dataDefinitions',
            'params' => null,
        ]);
    }

    /**
     * @Route("/transparency", name="transparency")
     */
    public function transparency(): Response {
        return $this->render('transparency.html.twig', [
            'svelteId' => 'dataProviders',
            'params' => '?listing=true&hide=transparency',
        ]);
    }

    /**
     * @Route("/tariffs", name="tariffs")
     */
    public function tariffs(): Response {
        return $this->render('tariffs.html.twig', [
            'svelteId' => 'tariffs',
            'params' => '?listing=true&hide=tariff',
        ]);
    }

    /**
     * @Route("/data-visualisation/{code1}/{code2}/{code3}/{code4}", name="data_visualisation")
     * TODO: CHANGE TO NEW MENU STRUCTURE
     */
    public function dataVisualisation(string $code1 = null, string $code2 = null, string $code3 = null, string $code4 = null): Response {
	    $title = 'Data Visualisation';
	    $svelteId = 'dataVisualisation';
	    $params = '?' . DefaultController::makeFilter($code2, $code3, $code4);
	    switch ($code1) {
		    case 'filling-levels':
			    $title .= ' / Filling Levels';
			    $params .= '&type=filling-levels';
			    break;
		    case 'filling-levels-region':
			    $title .= ' / Filling Levels / Region - Operator';
			    $svelteId = 'dataVisualisationRegion';
			    $params .= '&type=filling-levels';
			    break;
		    case 'net-withdrawal':
			    $title .= ' / Net Withdrawal';
			    $params .= '&type=net-withdrawal';
			    break;
		    case 'net-withdrawal-region':
			    $title .= ' / Net Withdrawal / Region - Operator';
			    $svelteId = 'dataVisualisationRegion';
			    $params .= '&type=net-withdrawal';
			    break;
		    case 'filling-levels-wgv':
			    $title = 'Storage Filling Levels / WGV';
			    $svelteId = 'dataVisualisationWGV';
			    $params = null;
			    break;
		    case 'filling-levels-country':
			    $title = 'Storage Filling Levels / Map';
			    $svelteId = 'dataVisualisationFillingLevels';
			    $params = null;
			    break;
		    case 'map':
			    $title = 'Storage Inventory / Map';
			    $svelteId = 'dataVisualisationMap';
			    $params = null;
			    break;
		    case 'flows':
			    $title = ' / Flows';
			    $svelteId = 'flows';
			    $params = null;
			    break;
		    default:
			    //nothing selected, redirect to EU
			    return $this->redirectToRoute('data_visualisation', [
				    'code1' => 'filling-levels',
				    'code2' => 'EU'
			    ]);
	    }
	    return $this->render('dataVisualisation.html.twig', [
		    'title' => $title,
		    'svelteId' => $svelteId,
		    'params' => $params,
	    ]);
    }

    /**
     * @Route("/disclaimer", name="disclaimer")
     */
    public function disclaimer(): Response {
        return $this->render('disclaimer.html.twig', [
            'svelteId' => 'disclaimer',
            'params' => null,
        ]);
    }

    /**
     * @Route("/privacy-policy", name="privacy_policy")
     */
    public function privacyPolicy(): Response {
        return $this->render('privacy.html.twig', [
            'svelteId' => 'privacy-policy',
            'params' => null,
        ]);
    }

    /**
     * @Route("/subscribe/{params?}", name="subscribe")
     */
    public function subscribe($params = null): Response {
        $message = null;
        if($params === 'thank-you') {
            //display thank you message
            $message = '<div class="alert alert-success alert-dismissible fade show text-center" role="alert"><p>Thank you for subscribing to the GIE Transparency Platform mailing list!</p><hr>
  <p class="mb-0 small">Kind regards,<br/>
the GIE Team<br/>
<br/>
www.agsi.gie.eu | Avenue de Cortenbergh, 100 - 1000 Brussels – Belgium</p><button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button></div>';
        }
        return $this->render('subscribe.html.twig', [
            'svelteId' => 'subscribe',
            'params' => '',
            'message' => $message,
        ]);
    }

    /**
     * @Route("/account", name="account")
     */
    public function account(Request $request): Response {
        $session = $request->getSession();
        // stores an attribute in the session for later reuse
        //$session->set('is_logged_in', []);
        // gets an attribute by name
        $logged_in = $session->get('is_logged_in');
        if($logged_in) {
            return $this->render('account.html.twig', [
                'svelteId' => 'account',
                'params' => json_encode($logged_in),
            ]);
        } else {
            return $this->render('login.html.twig', [
                'svelteId' => 'login',
                'params' => null,
            ]);
        }
    }
	
	/**
	 * @Route("/data-usage", name="data-usage")
	 */
	public function dataUsage(): Response {
		return $this->render('dataUsage.html.twig', [
			'svelteId' => 'data-usage',
			'params' => null,
		]);
	}

    /**
     * @Route("/maps", name="maps")
     */
    public function maps(): Response {
        return $this->render('maps.html.twig', [
            'svelteId' => 'maps',
            'params' => null,
        ]);
    }

    private function makeFilter(string $code1 = null, string $code2 = null, string $code3 = null, $code4 = false): string {
        $filter = '';
        //eu/non-eu/ai
        if (in_array(strtolower($code1), ['eu', 'ne', 'ai'])) {
            $filter = 'continent=' . strtoupper($code1);
        } else {
            if (!is_null($code3)) {
                //facility/country/company
                $filter = 'facility=' . strtoupper($code1) . '&country=' . strtoupper($code2) . '&company=' . strtoupper($code3);
            } else if (!is_null($code2)) {
                //company/country
                $filter = 'company=' . strtoupper($code1) . '&country=' . strtoupper($code2);
            } else if (!is_null($code1)) {
                //country
                $filter = 'country=' . strtoupper($code1);
            }
        }
        if($code4) {
            $filter .= '&extra=graphs';
        }
        return $filter;
    }
}