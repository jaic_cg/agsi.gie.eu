<?php
namespace App\EventListener;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

class ControllerListener implements EventSubscriberInterface {
    /**
     * @var Router
     */
    protected $router;

    /**
     * @param Router $router
     */
    public function __construct(RouterInterface $router) {
        $this->router = $router;
    }

    public static function getSubscribedEvents(): array {
        return [
            KernelEvents::EXCEPTION => 'onException',
        ];
    }

    public function onException(ExceptionEvent $event) {/*
        // implement custom logic and set your response, eg.:
        $url = $this->router->generate('account');
        $event->setResponse(new RedirectResponse($url));
        //return $this->redirectToRoute('account');*/
    }
}