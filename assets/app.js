/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/select2totree.css';
import './js/slick/slick.css';
import './js/slick/slick-theme.css';
import './styles/app.css';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
import './js/select2totree';
import './js/slick/slick.min';
//import './js/GaugeMeter/GaugeMeter';

import Account from "./js/Account.svelte";
import Dashboard from './js/Dashboard.svelte';
import DataDefinitions from './js/DataDefinitions.svelte';
import DataProviders from "./js/DataProviders.svelte";
import DataUsage from "./js/DataUsage.svelte";
import DataVisualisation from "./js/DataVisualisation.svelte";
import DataVisualisationMap from "./js/DataVisualisationMap.svelte";
import CapacityForecast from "./js/CapacityForecast.svelte";
import Flows from "./js/Flows.svelte";
import Gantt from "./js/UnavailabilityGantt.svelte";
import Graphs from "./js/Graphs.svelte";
import Historical from './js/Historical.svelte';
import Login from "./js/Login.svelte";
import Main from './js/Main.svelte';
import News from './js/News.svelte';
import Tariffs from "./js/Tariffs.svelte";
import Unavailability from './js/Unavailability.svelte';
import UnavailabilityMap from "./js/UnavailabilityMap.svelte";
import DataVisualisationFillingLevels from "./js/DataVisualisationFillingLevels.svelte";
import DataVisualisationWGV from "./js/DataVisualisationWGV.svelte";
import DataVisualisationRegion from "./js/DataVisualisationRegion.svelte";

if (document.getElementById('main') !== null) {
    const main = new Main({
        target: document.getElementById('main')//document.body,
    });
}
if (document.getElementById('dashboard') !== null) {
    const dashboard = new Dashboard({
        target: document.getElementById('dashboard')//document.body,
    });
}
if (document.getElementById('historical') !== null) {
    const historical = new Historical({
        target: document.getElementById('historical')//document.body,
    });
}
if (document.getElementById('unavailability') !== null) {
    const unavailability = new Unavailability({
        target: document.getElementById('unavailability')//document.body,
    });
}
if (document.getElementById('gantt') !== null) {
    const gantt = new Gantt({
        target: document.getElementById('gantt')//document.body,
    });
}
if (document.getElementById('unavailability_map') !== null) {
    const unavailability_map = new UnavailabilityMap({
        target: document.getElementById('unavailability_map')//document.body,
    });
}
if (document.getElementById('graphs') !== null) {
    const graphs = new Graphs({
        target: document.getElementById('graphs')//document.body,
    });
}
if (document.getElementById('news') !== null) {
    const news = new News({
        target: document.getElementById('news')//document.body,
    });
}
if (document.getElementById('dataProviders') !== null) {
    const dataProviders = new DataProviders({
        target: document.getElementById('dataProviders')//document.body,
    });
}
if (document.getElementById('dataDefinitions') !== null) {
    const dataDefinitions = new DataDefinitions({
        target: document.getElementById('dataDefinitions')//document.body,
    });
}
if (document.getElementById('capacityForecast') !== null) {
    const capacityForecast = new CapacityForecast({
        target: document.getElementById('capacityForecast')//document.body,
    });
}
if (document.getElementById('tariffs') !== null) {
    const tariffs = new Tariffs({
        target: document.getElementById('tariffs')//document.body,
    });
}
if (document.getElementById('dataVisualisation') !== null) {
    const dataVisualisation = new DataVisualisation({
        target: document.getElementById('dataVisualisation')//document.body,
    });
}
if (document.getElementById('dataVisualisationRegion') !== null) {
    const dataVisualisationRegion = new DataVisualisationRegion({
        target: document.getElementById('dataVisualisationRegion')//document.body,
    });
}
if (document.getElementById('dataVisualisationMap') !== null) {
    const dataVisualisationMap = new DataVisualisationMap ({
        target: document.getElementById('dataVisualisationMap')//document.body,
    });
}
if (document.getElementById('dataVisualisationFillingLevels') !== null) {
    const dataVisualisationFillingLevels = new DataVisualisationFillingLevels({
        target: document.getElementById('dataVisualisationFillingLevels')//document.body,
    });
}
if (document.getElementById('dataVisualisationWGV') !== null) {
    const dataVisualisationWGV = new DataVisualisationWGV({
        target: document.getElementById('dataVisualisationWGV')//document.body,
    });
}
if (document.getElementById('flows') !== null) {
    const flows = new Flows({
        target: document.getElementById('flows')//document.body,
    });
}
if(document.getElementById('data-usage') !== null) {
    const dataUsage = new DataUsage({
        target: document.getElementById('data-usage')//document.body,
    });
}
if (document.getElementById('account') !== null) {
    const account = new Account({
        target: document.getElementById('account')//document.body,
    });
}
if (document.getElementById('login') !== null) {
    const login = new Login({
        target: document.getElementById('login')//document.body,
    });
}

$('.carousel').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    draggable: false,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 6000,
    adaptiveHeight: true,
    adaptiveWidth: true,
});


$(document).on('click', '.title-tooltip', function (el) {
    let tooltip = '#tooltip-screen',
        text = $(this).attr('title'),
        width = $(this).parents('.tabulator-col:first').width() - 25;
    $(tooltip).css({top: el.pageY + 15, left: el.pageX - width, position: 'absolute'}).html(text).toggle();
});
$(document).on('mouseleave', '.tabulator-col', function (el) {
    let tooltip = '#tooltip-screen';
    $(tooltip).html('').hide();
});