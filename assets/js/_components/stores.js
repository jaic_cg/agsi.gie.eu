import { writable } from 'svelte/store';

export const token = writable('');
export const slug = writable('');
export const first_name = writable('');
export const last_name = writable('');
export const email = writable('');
export const api_key = writable('');
export const api_exp = writable('');
export const role = writable(0);
export const mail_api = writable(0);
export const api_access = writable(0);
export const company = writable('');
export const telephone = writable('');